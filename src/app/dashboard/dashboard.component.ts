import { Component, OnInit } from '@angular/core';
import { DashboardService } from "../services/DashboardService/dashboard.service";
import { AuthService } from "../services/AuthService/auth.service";
import Swal from 'sweetalert2'
import { ToastrService } from 'ngx-toastr';
import { Product } from "../classes/product";
import { Router } from '@angular/router';
import { Categories } from '../classes/categories';
import { MatDialog } from '@angular/material';
import { LogisticComponent } from '../logistic/logistic.component'
import { SaleDetailService } from '../services/SaleDetail/sale-detail.service';
import { CustomerService } from '../services/CustomerService/customer.service';
import { CustomerData } from '../classes/customerData';
import { LogisticService } from '../services/sharedServices/logistic.service';
import { SustituteProdDialogComponent } from '../sustitute-prod-dialog/sustitute-prod-dialog.component';
import { AdmImageService, sustituteProd } from '../services/AdmImageService/adm-image.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  products: Product[] = [];
  searchText: string = '';
  productsAdded: string = '0';
  categories: Categories[] = [];
  originalProducts: Product[] = [];
  customerData: CustomerData;
  sustituteProds: sustituteProd[] = [];

  //charge
  isLoad: boolean = false;
  isSustitute: boolean = true;

  constructor(
    private dataService: DashboardService,
    private authService: AuthService,
    private toastr: ToastrService,
    private router: Router,
    public dialog: MatDialog,
    private detailService: SaleDetailService,
    private customerService: CustomerService,
    private _logisticService: LogisticService,
    private _admImgService: AdmImageService) { }

  ngOnInit() {
    this._logisticService.setWareHouseName();
    this._logisticService.setDateLogistic();
    this._logisticService.setNamOrderLogistic();
    if (this.authService.getsesionLogistic() != '1') {
      this.getCustomerData()
    } else {
      this.getCatalogue(this.authService.getDoc(), this.authService.getTransactionId(), Number(this.authService.getValueLogistic()), this.authService.getWereHouse());
      this.getCategories();
    }
  }

  openDialogLogistic(): void {
    let dialogRef = this.dialog.open(LogisticComponent, {
      'width': '100vw',
      'height': '90vh',
      data: this.customerData
    });

    dialogRef.afterClosed().subscribe(res => {
      if (!res) {
        this.openDialogLogistic();
        return;
      }
      if (res == 'Logout') {
        this.logOutSale();
      }
      this.authService.sesionLogisticc();
      this.authService.valueLogisticc(res.orderLogistic);
      this.authService.setWereHouse(res.wareHouse);
      this.authService.setWareHouseName(res.wareHouseName);
      this.authService.seTNamOrderLogistic(res.namOrderLogistic);
      this.getCatalogue(this.authService.getDoc(), this.authService.getTransactionId(), Number(this.authService.getValueLogistic()), this.authService.getWereHouse());
      this.getCategories();

      //recoger pedido
      if (res.orderLogistic == 1) {
        if (res.collectedDate) { // si hay fecha de recogida seleccionada desde el model de logistica, se actualiza la fecha del pedido
          const logisticDate: Date = res.collectedDate;
          const deliveryLogistics: string = res.namOrderLogistic;
          const priceGroup: number = res.priceList;
          this.authService.setDateLogistic(res.collectedDate);
          //servicio actualiza la fecha de entrega y tipoDocumentoPsl
          this.detailService.updateLogisticDate(this.authService.getTransactionId(), logisticDate, 'PVTCR', deliveryLogistics, priceGroup).subscribe(
            (response) => {
              if (response['error']) {
                switch (response['code']) {
                  case 101:
                    this.showFailedToaster(response['msg']);
                    break;
                  case 104:
                    this.showFailedToaster(response['msg']);
                    break;
                  case 106:
                    this.showInfoToaster(response['msg']);
                    break;
                  default:
                    this.showInfoToaster(response['msg']);
                }
              }
            }
          );
        }
      } else if (res.orderLogistic == 12) { //Domicilio
        if (res.deliveryDate) {
          const deliveryLogistics: string = res.namOrderLogistic;
          const priceGroup: number = res.priceList;
          this.authService.setDateLogistic(res.deliveryDate);
          //servicio actualiza la fecha de entrega y tipoDocumentoPsl
          this.detailService.updateLogistiDate(this.authService.getTransactionId(), res.deliveryDate, deliveryLogistics, priceGroup).subscribe(
            (response) => {
              if (response['error']) {
                switch (response['code']) {
                  case 101:
                    this.showFailedToaster(response['msg']);
                    break;
                  case 104:
                    this.showFailedToaster(response['msg']);
                    break;
                  case 106:
                    this.showInfoToaster(response['msg']);
                    break;
                  default:
                    this.showInfoToaster(response['msg']);
                }
              }
            }
          );
        }
      }
      this._logisticService.setWareHouseName();
      this._logisticService.setDateLogistic();
      this._logisticService.setNamOrderLogistic();
    });
  }

  redirectToProducts() {
    this.router.navigate(['Venta/TusProductos']);
  }

  getCustomerData() {
    this.customerService.getCustomerData(this.authService.getDoc())
      .subscribe(
        (response) => {
          if (response['error']) {
            switch (response['code']) {
              case 101:
                this.ShowErrorDialog(response['msg']); //Error en información de usuario.
                this.authService.goToLogin();
                break;
              case 102:
                this.ShowWarningDialog(response['msg']); //No hay datos del cliente vinculados a tu información de usuario.
                break;
            }
          } else {
            this.customerData = response['customerDat'][0];
            this.openDialogLogistic();
          }
        }
      );
  }

  openDialogSustProduct(idProduct) {
    this._admImgService.sustituteProds(idProduct)
      .subscribe((data) => {
        this.sustituteProds = data['sustiuteProds'];

        if (!(this.sustituteProds.length > 0)) {
          this.ShowInfoDialog('No se encontraron productos sustitutos');
        } else {
          const _nameSustiProduc = this.sustituteProds.map(s => s.descp);
          const _sustituteProducts = this.originalProducts.filter(p => _nameSustiProduc.indexOf(p.DescripcionProducto) != -1);
          const dialogRef = this.dialog.open(SustituteProdDialogComponent, {
            width: '90vw',
            height: '90vh',
            data: _sustituteProducts,
            disableClose: true
          });

          dialogRef.afterClosed().subscribe(res => {
            // if (!res) {
            //   return;
            // }
            this.getCatalogue(this.authService.getDoc(), this.authService.getTransactionId(), Number(this.authService.getValueLogistic()), this.authService.getWereHouse());
          });
        }
      });
  }

  ShowInfoDialog(msg: string) {
    Swal.fire({
      title: 'Información',
      text: msg,
      type: 'info',
      showCancelButton: false,
      confirmButtonText: 'Ok, Entiendo',
      confirmButtonColor: '#FF5E4F',
    });
  }

  ShowWarningDialog(msg: string) {
    Swal.fire({
      title: 'Atención',
      text: msg,
      type: 'warning',
      showCancelButton: false,
      confirmButtonText: 'Ok, Entiendo',
      confirmButtonColor: '#FF5E4F',
    });
  }

  ShowErrorDialog(msg: string) {
    Swal.fire({
      title: 'Atención',
      text: msg,
      type: 'error',
      showCancelButton: false,
      confirmButtonText: 'Ok, Entiendo',
      confirmButtonColor: '#FF5E4F',
    });
  }

  insertProduct(product) {
    this.dataService.insertDetail(product[0], this.authService.getUser(), this.authService.getDoc(), this.authService.getTransactionId())
      .subscribe(
        (response) => {
          if (response['error']) {
            switch (response['code']) {
              case 103:
                this.showFailedToaster(response['msg']);
                break;
              case 104:
                this.showFailedToaster(response['msg']);
                break;
              case 105:
                this.showInfoToaster(response['msg']);
                break;
              case 106:
                this.showFailedToaster(response['msg']);
                break;
              default:
                this.showInfoToaster(response['msg']);
            }
          } else {
            this.showSuccessToaster(response['msg']);
            let itemToUpdate = this.products.findIndex(
              productList => productList.IdProducto == product[1].IdProducto
            );
            this.products[itemToUpdate] = product[1];
            this.refreshQuantityAdded();
          }
        }
      );
  }

  deleteProduct(product) {
    this.dataService.deleteProduct(product[0], this.authService.getUser(), this.authService.getDoc(), this.authService.getTransactionId())
      .subscribe(
        (response) => {
          if (response['error']) {
            this.showFailedToaster(response['msg']);
          } else {
            this.showSuccessToaster(response['msg']);
            let itemToUpdate = this.products.findIndex(
              productList => productList.IdProducto == product[1].IdProducto
            );
            this.products[itemToUpdate] = product[1];
            this.refreshQuantityAdded();
          }
        }
      );
  }

  searchTextInList(text: string) {
    this.searchText = text;
  }

  showSuccessToaster(msg: string) {
    this.toastr.success(msg);
  }

  showInfoToaster(msg: string) {
    this.toastr.warning(msg);
  }

  showFailedToaster(msg: string) {
    this.toastr.error(msg);
  }

  private refreshQuantityAdded() {
    this.productsAdded = this.products.map(product => product.Presentaciones)
      .reduce((totalQuantity, quantity) =>
        String(Number(totalQuantity) +
          Number(
            quantity.reduce((quantityAllPresentations, presentationSingle) =>
              String(Number(quantityAllPresentations) + (Number(presentationSingle.CantidadAgregada) > 0 ? 1 : 0))
              , '0')
          )
        )
        , '0');
  }

  getCatalogue(doc: string, idTransaction: string, valueLogistic: number, wareHouse: string) {
    this.isLoad = true;
    this.dataService.getCatalogue(doc, idTransaction, valueLogistic, wareHouse)
      .subscribe(
        (response) => {
          if (response['error']) {
            switch (response['code']) {
              case 101:
                this.ShowErrorDialog(response['msg']); //Error en información de usuario.
                this.authService.goToLogin();
                break;
              case 102:
                this.ShowWarningDialog(response['msg']); //No hay productos vinculados a tu información de usuario.
                break;
            }
          } else {
            this.products = response['Catalogue'];
            this.originalProducts = response['Catalogue'];
            this.refreshQuantityAdded();
          }
          this.isLoad = false;
        }
      );
  }

  getCategories() {
    this.dataService.getCategories(this.authService.getDoc())
      .subscribe(
        (response) => {
          if (response['error']) {
            switch (response['code']) {
              case 101:
                this.ShowErrorDialog(response['msg']); //Error en información de usuario.
                this.authService.goToLogin();
                break;
              case 102:
                this.ShowWarningDialog(response['msg']); //No hay productos vinculados a tu información de usuario.
                break;
            }
          } else {
            this.categories = response['categories'];
          }
        }
      );
  }

  logOutSale() {
    this.dataService.cancelSale(this.authService.getTransactionId())
      .subscribe((response) => {
        if (response['error']) {
          this.showFailedToaster(response['msg']);
        } else {
          this.showSuccessToaster(response['msg']);
          this.authService.goToLogin();
        }
      });
  }

  getSelectedCategories(categories: any) {
    this.products = this.originalProducts;
    var filterCat = [];

    filterCat = categories.value.map((item) => item.CodGrupo);

    if (filterCat.length > 0) {
      this.products = this.products = this.products.filter(products => filterCat.indexOf(products.CodGrupo) != -1);
    }
  }
}
