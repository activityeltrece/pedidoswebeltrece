import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AdmImageService, sustituteProd, Products } from '../services/AdmImageService/adm-image.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-config-sustitute-prod-dialog',
  templateUrl: './config-sustitute-prod-dialog.component.html',
  styleUrls: ['./config-sustitute-prod-dialog.component.css']
})
export class ConfigSustituteProdDialogComponent implements OnInit {

  sustituteProds: sustituteProd[] = [];
  products: Products[] = [];

  constructor(
    public dialogRef: MatDialogRef<ConfigSustituteProdDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public idProduct: number,
    private _admImgService: AdmImageService,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.getAllproducts();
    this.getSustituteProds();
  }

  getSustituteProds() {
    this._admImgService.sustituteProds(this.idProduct)
      .subscribe((data) => {
        this.sustituteProds = data['sustiuteProds'];
      });
  }

  getAllproducts() {
    this._admImgService.allProducts()
      .subscribe((response) => {
        if(response['error']){
          this.showFailedToaster(response['msg']);
        }else{
          this.products = response['products'];
        }
      });
  }

  removeSusProd(idSustProd: number) {
    this._admImgService.removeSusProd(idSustProd).
      subscribe((response) => {
        if (response['error']) {
          this.showFailedToaster(response['msg'])
        } else {
          this.showSuccessToaster(response['msg']);
          this.getSustituteProds();
        }
      })
  }

  newSusProd(idSustProd: number){
    this._admImgService.insertSusProd(this.idProduct,idSustProd)
    .subscribe((response)=>{
      if(response['error']){
        this.showFailedToaster(response['msg']);
      }else{
        this.showSuccessToaster(response['msg']);
        this.getSustituteProds();
      }
    });
  }

  showFailedToaster(msg: string) {
    this.toastr.error(msg);
  }

  showSuccessToaster(msg: string) {
    this.toastr.success(msg);
  }
}


