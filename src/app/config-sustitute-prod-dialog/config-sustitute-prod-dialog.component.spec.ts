import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigSustituteProdDialogComponent } from './config-sustitute-prod-dialog.component';

describe('ConfigSustituteProdDialogComponent', () => {
  let component: ConfigSustituteProdDialogComponent;
  let fixture: ComponentFixture<ConfigSustituteProdDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigSustituteProdDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigSustituteProdDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
