import { Pipe, PipeTransform } from '@angular/core';
import { Product } from "../classes/product";

@Pipe({
  name: 'search',
  pure: false
})
export class SearchPipe implements PipeTransform {

  transform(items: Product[], searchText: string): Product[] {

    if (!items) {
      return [];
    }
    if (!searchText) {
      return items;
    }

    return items.filter(
      (product: Product) => product.DescripcionProducto.toLowerCase().includes(searchText.toLowerCase()) || product.IdProducto.toLowerCase().includes(searchText.toLowerCase())
    );

  }

}