import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterImagesP'
})
export class FilterPipe implements PipeTransform {

  transform(value: any, ...arg: any): any {
    const resultproducts = [];
    for(const products of value){
        if(products.descripcion.toLowerCase().indexOf(arg.toString().toLowerCase()) > -1
        ){
          resultproducts.push(products)
        };
    };
    return resultproducts;
  }

}
