import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaleDetailStepperComponent } from './sale-detail-stepper.component';

describe('SaleDetailStepperComponent', () => {
  let component: SaleDetailStepperComponent;
  let fixture: ComponentFixture<SaleDetailStepperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaleDetailStepperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaleDetailStepperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
