import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { WareHouse } from '../classes/warehouse';
import { OrderLogistic } from '../classes/order_logistic';

@Component({
  selector: 'app-sale-detail-stepper',
  templateUrl: './sale-detail-stepper.component.html',
  styleUrls: ['./sale-detail-stepper.component.css']
})
export class SaleDetailStepperComponent implements OnInit {

  @Input() public countProducts: number;
  @Input() public totalValue: number;
  @Input() public totalValueIVA: number;
  @Input() public totalProducts: number;
  @Input() public sumQuantity: number;
  @Output() finishSale = new EventEmitter();
  @Output() inlogistic = new EventEmitter();
  maxlengthObservation: number = 300;
  observationText: string = '';


  constructor() { }

  ngOnInit() {
    this.infoLogistic();
  }

  sendSale(observation: string) {
    this.finishSale.emit({observation});
  }

  infoLogistic(){
    this.inlogistic.emit();
  }

}
