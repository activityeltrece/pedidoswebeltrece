import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SaleProduct } from 'src/app/classes/sale_product';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-detail-list',
  templateUrl: './detail-list.component.html',
  styleUrls: ['./detail-list.component.css']
})
export class DetailListComponent implements OnInit {

  @Input() public salesProduct: SaleProduct[] = [];

  @Input() public totalUnitaryValue: number;
  @Input() public totalValue: number;
  @Input() public totalValueIVA: number;
  @Input() public totalProducts: number;
  @Input() public sumQuantity: number;

  @Output() deleteSaleProduct = new EventEmitter();
  @Output() updateSaleProduct = new EventEmitter();
  displayedColumns = ['DescripcionProducto', 'Cantidad', 'ValorUnitario', 
  'ValorBruto', 'ValorIva', 'TotalPedidoArticulo', 'DeleteOption'];
  minQuantity: number = 1;
  maxQuantity: number = 999;

  constructor() { }

  ngOnInit() {
  }

  deleteSaleProductOfList(productSaleDetail: SaleProduct) {
    this.deleteSaleProduct.emit(productSaleDetail);
  }

  updateSaleProductOfList(productSaleDetail: SaleProduct, quantity){
    if( !quantity || !Number.isInteger((Number(quantity))) || (Number(quantity) < this.minQuantity) || (Number(quantity) > this.maxQuantity) ){
      this.ShowWarningDialog(`La cantidad ingresada no es válida. (Permitido: ${this.minQuantity} a ${this.maxQuantity})`);
      return;
    }else {
      let totals = this.mathProcess(productSaleDetail,quantity);
      this.updateSaleProduct.emit({
        idProduct: productSaleDetail.IdProducto,
        quantity,
        unitaryValue: productSaleDetail.ValorUnitario, //valor_unitario
        presentation: productSaleDetail.UnidadPresentacion,
        total: totals.Total, //total_pedido_articulo & totalPrecioNeto
        discount: 0, //descuentoEscala
        iva: Number(productSaleDetail.IVA),
        valueIVA: totals.ValorIva, //vlr_iva
        totalBruto: totals.TotalBruto, //valor_bruto
        unitaryValueNet: totals.UnitaryValueNet, //ValorUnitarioNeto
        unitaryValueIva: totals.UnitaryValueIva //ValorIvaUnitario
      });
    }
  }

  mathProcess(saleProductToUpdate:SaleProduct, quantity){
    let TotalBruto: number = (Number(quantity) * Math.round(Number(saleProductToUpdate.ValorUnitario)));
    let ValorIva: number = (TotalBruto * Number(saleProductToUpdate.IVA)) / 100;
    let UnitaryValueIva: number = ValorIva / Number(quantity);
    let TotalIva: number = (((Number(saleProductToUpdate.IVA) * Math.round(Number(saleProductToUpdate.ValorUnitario))) / 100) * Number(quantity));
    let Total: number = TotalBruto + TotalIva;
    let UnitaryValueNet: number = Total / Number(quantity);
    return {
      TotalBruto,
      ValorIva,
      UnitaryValueIva,
      TotalIva,
      UnitaryValueNet,
      Total
    };
  }

  ShowWarningDialog(msg:string){
    Swal.fire({
      title: 'Atención',
      text: msg,
      type: 'warning',
      showCancelButton: false,
      confirmButtonText: 'Ok, Entiendo',
      confirmButtonColor: '#FF5E4F',
    });
  }

}
