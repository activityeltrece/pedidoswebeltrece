import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { SaleProduct } from 'src/app/classes/sale_product';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  @Input() public saleProduct: SaleProduct;
  @Output() deleteSaleProduct = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }
  
  deleteSaleProductList(){
    this.deleteSaleProduct.emit(this.saleProduct);
  }

}
