import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DetailProduct } from "../../classes/detail_product";
import { Product } from "../../classes/product";
import Swal from 'sweetalert2'

@Component({
  selector: 'product-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

  @Input() public product: Product;
  @Input() public isSustitute: boolean;
  @Output() addProduct = new EventEmitter();
  @Output() deleteProduct = new EventEmitter();
  @Output() openDialogSustProd = new EventEmitter();

  presentation: DetailProduct;
  quantity: string = '';
  minQuantity: number = 1;
  maxQuantity: number = 999;
  inventary: string = '';
  constructor(
  ) {

  }

  ngOnInit() {
    this.presentation = this.product.Presentaciones[0];
    this.inventary = this.product.Presentaciones[0].Inventary ? this.product.Presentaciones[0].Inventary.toString() : '0';
    this.quantity = this.product.Presentaciones[0].CantidadAgregada > 0 ? this.product.Presentaciones[0].CantidadAgregada.toString() : '';
  }

  checkData() {
    if (!this.quantity || !Number.isInteger((Number(this.quantity))) || (Number(this.quantity) < this.minQuantity) || (Number(this.quantity) > this.maxQuantity)) {
      this.quantity = '';
      this.ShowWarningDialog(`La cantidad ingresada no es válida. (Permitido: ${this.minQuantity} a ${this.maxQuantity})`);
      return;
    } else {
      let totals = this.mathProcess();
      this.addProduct.emit(
        [
          {
            idProduct: this.product.IdProducto,
            quantity: this.quantity,
            unitaryValue: Math.round(Number(this.presentation.PrecioVenta)), //valor_unitario
            presentation: this.presentation.UnidadPresentacion,
            total: totals.Total, //total_pedido_articulo & totalPrecioNeto
            discount: 0, //descuentoEscala
            iva: Number(this.product.IVA),
            valueIVA: totals.ValorIva, //vlr_iva
            totalBruto: totals.TotalBruto, //valor_bruto
            unitaryValueNet: totals.UnitaryValueNet, //ValorUnitarioNeto
            unitaryValueIva: totals.UnitaryValueIva //ValorIvaUnitario
          },
          this.productWithNewQuantity(false)
        ]

      );
    }
  }

  productWithNewQuantity(isDeleted: boolean) {
    let index = this.product.Presentaciones.findIndex(
      presentationList => presentationList.UnidadPresentacion == this.presentation.UnidadPresentacion
    );
    let auxProduct: Product = JSON.parse(JSON.stringify(this.product));
    auxProduct.Presentaciones[index].CantidadAgregada = isDeleted ? 0 : Number(this.quantity);
    auxProduct.Presentaciones.sort((a, b) => b.CantidadAgregada - a.CantidadAgregada);
    return auxProduct;
  }

  delete() {
    this.deleteProduct.emit(
      [
        {
          idProduct: this.product.IdProducto,
          presentation: this.presentation.UnidadPresentacion,
        },
        this.productWithNewQuantity(true)
      ]
    );
  }

  mathProcess() {
    let TotalBruto: number = (Number(this.quantity) * Math.round(Number(this.presentation.PrecioVenta)));
    let ValorIva: number = (TotalBruto * Number(this.product.IVA)) / 100;
    let UnitaryValueIva: number = ValorIva / Number(this.quantity);
    let TotalIva: number = (((Number(this.product.IVA) * Math.round(Number(this.presentation.PrecioVenta))) / 100) * Number(this.quantity));
    let Total: number = TotalBruto + TotalIva;
    let UnitaryValueNet: number = Total / Number(this.quantity);
    return {
      TotalBruto,
      ValorIva,
      UnitaryValueIva,
      TotalIva,
      UnitaryValueNet,
      Total
    };

  }

  checkPresentation() {
    this.quantity = this.presentation.CantidadAgregada > 0 ? this.presentation.CantidadAgregada.toString() : '';
  }

  openDialogProdSus(idProduct) {
    this.openDialogSustProd.emit(idProduct);
  }


  ShowWarningDialog(msg: string) {
    Swal.fire({
      title: 'Atención',
      text: msg,
      type: 'warning',
      showCancelButton: false,
      confirmButtonText: 'Ok, Entiendo',
      confirmButtonColor: '#FF5E4F',
    });
  }

  getValorIVA() {
    return Number(this.presentation.PrecioVentaIVA) - Number(this.presentation.PrecioVenta);
  }

}
