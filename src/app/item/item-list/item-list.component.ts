import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Product } from "../../classes/product";

@Component({
  selector: 'products-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.css']
})
export class ItemListComponent implements OnInit {

  @Input() public products: Product[] = [];
  @Input() public isSustitute: boolean;
  @Output() insertProduct = new EventEmitter();
  @Output() deleteProduct = new EventEmitter();
  @Output() openDialogSustProduct = new EventEmitter();
  @Input() public searchText: string = '';

  constructor() { }

  ngOnInit() {
  }

  addProductList( productDetail ){
    this.insertProduct.emit(productDetail);
  }

  deleteProductList( productDetail ){
    this.deleteProduct.emit(productDetail);
  }

  openDialogSustProd(idProduct){
    this.openDialogSustProduct.emit(idProduct);
  }

}
