export interface CustomerData {
    nit: string;
    cedula: string;
    razon_social: string;
    direccion: string;
    telefono: string;
    cupo: number;
    CupoDisponible: number;
    CupoUtilizado: number;
}
