export interface DetailProduct {
    UnidadPresentacion: string;
    PrecioVenta: string;
    PrecioVentaIVA: string;
    CantidadAgregada: number;
    Inventary: number
}