export interface detailOrder {
    UnidadPresentacion : string;
    cantidad : string;
    descripcion : string;
    id_pedido : string
    id_producto : string;
    total_pedido_articulo : string; 
    valor_unitario : string;
    vlr_iva : string;
}