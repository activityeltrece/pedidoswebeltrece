export interface SaleHeader {
    ValorPedido: string;
    TotalPedido: string;
    ValorBruto: string;
    ValorIva: string;
    CodigoBodega: string;
    Observacion: string;
}