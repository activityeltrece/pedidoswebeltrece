export interface SaleProduct {
    IdProducto: string;
    DescripcionProducto: string;
    Cantidad: string;
    UnidadPresentacion: string;
    ValorUnitario: string;
    ValorBruto: string;
    ValorIva: string;
    TotalPedidoArticulo: string;
    DescuentoEscala: string;
    IVA: string;
}