import { detailOrder } from './detail_order';

export interface HistoricalOrders {
    id_pedido:string;
    fecha_pedido: string;
    cedula_cliente: string;
    valor_pedido: string;
    hora_final:string
    observacion:string;
    total_pedido:string;
    ProductsDetail : detailOrder[]; 
}