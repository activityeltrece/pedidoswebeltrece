import { DetailProduct } from "../classes/detail_product";
export interface Product {
    IdProducto: string;
    DescripcionProducto: string;
    IVA: string;
    CodSegmentoPrecio: string;
    PathImage: string;
    ImageDesc: string;
    CodGrupo: string;
    DescripCateg: string;
    Presentaciones: DetailProduct[];
}