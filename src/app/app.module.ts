import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from "../app/material.module";
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { ParticlesModule } from 'angular-particle';
import { ToastrModule } from 'ngx-toastr';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AuthGuard } from "./services/Guards/auth-guard.service";
import { ToolbarComponent } from './toolbar/toolbar.component';
import { ItemComponent } from './item/item/item.component';
import { ItemListComponent } from './item/item-list/item-list.component';
import { SearchPipe } from './pipes/search.pipe';
import { SaleDetailComponent } from './sale-detail/sale-detail.component';
import { DetailListComponent } from './item/detail-list/detail-list.component';
import { DetailComponent } from './item/detail/detail.component';
import { LoginGuard } from './services/Guards/login-guard.service';
import { ToolbarSaleDetailComponent } from './toolbar-sale-detail/toolbar-sale-detail.component';
import { SaleDetailStepperComponent } from './sale-detail-stepper/sale-detail-stepper.component';
import { HistoricalOrdersComponent } from './historical-orders/historical-orders.component';
import { DetailOrdersComponent } from './detail-orders/detail-orders.component';
import { MainNavComponent } from './main-nav/main-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { AdmImgComponent } from './adm-img/adm-img.component';
import { FilterPipe } from './pipes/filter-images-p.pipe';
import { ImgAdminGuard } from './services/Guards/img-admin-guard.service';
import { LogisticComponent } from './logistic/logistic.component';
import { ConfigSustituteProdDialogComponent } from './config-sustitute-prod-dialog/config-sustitute-prod-dialog.component';
import { SustituteProdDialogComponent } from './sustitute-prod-dialog/sustitute-prod-dialog.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    PageNotFoundComponent,
    ToolbarComponent,
    ItemComponent,
    ItemListComponent,
    SearchPipe,
    SaleDetailComponent,
    DetailListComponent,
    DetailComponent,
    ToolbarSaleDetailComponent,
    SaleDetailStepperComponent,
    HistoricalOrdersComponent,
    DetailOrdersComponent,
    MainNavComponent,
    AdmImgComponent,
    FilterPipe,
    LogisticComponent,
    ConfigSustituteProdDialogComponent,
    SustituteProdDialogComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    SweetAlert2Module.forRoot(),
    ParticlesModule,
    ToastrModule.forRoot(),
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule
  ],
  entryComponents: [LogisticComponent, ConfigSustituteProdDialogComponent, SustituteProdDialogComponent],
  providers: [AuthGuard, LoginGuard, ImgAdminGuard],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
