import { Component, OnInit } from '@angular/core';
import { SaleDetailService } from "../services/SaleDetail/sale-detail.service";
import { AuthService } from "../services/AuthService/auth.service";
import { ToastrService } from 'ngx-toastr';
import { SaleProduct } from '../classes/sale_product';
import { DashboardService } from '../services/DashboardService/dashboard.service';
import { LoginService } from '../services/LoginService/login.service';
import { User } from '../classes/user';
import { LogisticService } from '../services/sharedServices/logistic.service';

@Component({
  selector: 'app-sale-detail',
  templateUrl: './sale-detail.component.html',
  styleUrls: ['./sale-detail.component.css']
})
export class SaleDetailComponent implements OnInit {

  salesProduct: SaleProduct[] = [];
  user: User;
  constructor(private detailService: SaleDetailService, private dataService: DashboardService, private authService: AuthService,
    private toastr: ToastrService, private loginService: LoginService, private _logisticService: LogisticService) { }

  ngOnInit() {
    this.detailService.getSaleDetail(this.authService.getTransactionId())
      .subscribe(
        (response) => {
          if (response['error']) {
            switch (response['code']) {
              case 101:
                this.showFailedToaster(response['msg']);
                break;
              case 104:
                this.showInfoToaster(response['msg']);
                break;
            }
          } else {
            this.salesProduct = response['details'];
          }
        }
      );
  }

  showSuccessToaster(msg: string) {
    this.toastr.success(msg);
  }

  showInfoToaster(msg: string) {
    this.toastr.warning(msg);
  }

  showFailedToaster(msg: string) {
    this.toastr.error(msg);
  }

  deleteSaleProduct(productSaleDetail: SaleProduct) {
    this.dataService.deleteProduct(
      {
        idProduct: productSaleDetail.IdProducto,
        presentation: productSaleDetail.UnidadPresentacion
      }, this.authService.getUser(), this.authService.getDoc(), this.authService.getTransactionId())
      .subscribe(
        (response) => {
          if (response['error']) {
            this.showFailedToaster(response['msg']);
          } else {
            this.showSuccessToaster(response['msg']);
            this.salesProduct = this.salesProduct.filter((saleProduct) => {
              return !(saleProduct.IdProducto == productSaleDetail.IdProducto && saleProduct.UnidadPresentacion == productSaleDetail.UnidadPresentacion);
            });
          }
        }
      );
  }

  updateSaleProduct(productSaleToUpdate: any) {
    this.dataService.updateProduct(productSaleToUpdate, this.authService.getUser(), this.authService.getDoc(), this.authService.getTransactionId())
      .subscribe(
        (response) => {
          if (response['error']) {
            switch (response['code']) {
              case 103:
                this.showFailedToaster(response['msg']);
                break;
              case 104:
                this.showFailedToaster(response['msg']);
                break;
              case 106:
                this.showFailedToaster(response['msg']);
                break;
              default:
                this.showInfoToaster(response['msg']);
            }
          } else {
            this.showSuccessToaster(response['msg']);
            let productUpdatedIndex = this.salesProduct.findIndex(
              productList => { return productList.IdProducto == productSaleToUpdate.idProduct && productList.UnidadPresentacion == productSaleToUpdate.presentation; }
            );
            this.updateDataProduct(productUpdatedIndex, productSaleToUpdate);
          }
        }
      );
  }

  private updateDataProduct(index: number, newData: any) {
    this.salesProduct[index].Cantidad = newData.quantity;
    this.salesProduct[index].TotalPedidoArticulo = newData.total;
    this.salesProduct[index].ValorBruto = newData.totalBruto;
    this.salesProduct[index].ValorIva = newData.valueIVA;
    this.salesProduct[index].ValorUnitario = newData.unitaryValue;
  }

  redirectToDashboard() {
    this.authService.goToDashboard();
  }

  getTotalQuantity() {
    return this.salesProduct.length;
  }

  getSumQuantity() {
    return this.salesProduct.map(product => product.Cantidad)
      .reduce((totalQuantityValue, quantityValue) => String(Number(totalQuantityValue) + Number(quantityValue)), '0');
  }

  getTotalUnitaryValue() {
    return this.salesProduct.map(product => product.ValorUnitario)
      .reduce((totalUnitaryValue, unitaryValue) => String(Number(totalUnitaryValue) + Number(unitaryValue)), '0');
  }

  getTotalValue() {
    return this.salesProduct.map(product => product.ValorBruto)
      .reduce((totalValue, value) => String(Number(totalValue) + Number(value)), '0');
  }

  getTotalValueIVA() {
    return this.salesProduct.map(product => product.ValorIva)
      .reduce((totalValueIVA, valueIVA) => String(Number(totalValueIVA) + Number(valueIVA)), '0');
  }

  getTotalProducts() {
    return this.salesProduct.map(product => product.TotalPedidoArticulo)
      .reduce((totalProductValue, productValue) => String(Number(totalProductValue) + Number(productValue)), '0');
  }

  sendSale(wareHouse_and_observation: any) {
    this.detailService.finishSale(this.authService.getTransactionId(), {
      ValorPedido: this.getTotalProducts(),
      TotalPedido: this.getSumQuantity(),
      ValorBruto: this.getTotalValue(),
      ValorIva: this.getTotalValueIVA(),
      CodigoBodega: this.authService.getWereHouse(),
      Observacion: wareHouse_and_observation.observation
    }).subscribe(
      (response) => {
        if (response['error']) {
          this.showFailedToaster(response['msg']);
        } else {
          this.showSuccessToaster(response['msg']);
          //this.authService.goToLogin();
          this.generateId();
        }
      }
    );
  }

  infologistic(){
    //this._logisticService.setWareHouseName();
    //this._logisticService.setDateLogistic();
    //this._logisticService.setNamOrderLogistic();
  }


  generateId() {
    this.loginService.generateId(this.authService.getUser())
      .subscribe(
        (response) => {
          if (response['error']) {
            this.showFailedToaster(response['msg']);
          } else {
            this.authService.FinishSaleAndRedirectVentas(response['token'], this.authService.getUser(), response['doc'], response['TransactionID']);
          }
        }
      );
  }

}
