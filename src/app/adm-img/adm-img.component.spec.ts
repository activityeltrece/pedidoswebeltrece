import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmImgComponent } from './adm-img.component';

describe('AdmImgComponent', () => {
  let component: AdmImgComponent;
  let fixture: ComponentFixture<AdmImgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdmImgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmImgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
