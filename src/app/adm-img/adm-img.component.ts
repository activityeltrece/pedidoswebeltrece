import { Component, Input, OnInit } from '@angular/core';
import Swal from 'sweetalert2'
import { ProductsImg } from '../classes/productsImg';
import { AdmImageService } from '../services/AdmImageService/adm-image.service';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { ConfigSustituteProdDialogComponent } from 'src/app/config-sustitute-prod-dialog/config-sustitute-prod-dialog.component';

@Component({
  selector: 'app-adm-img',
  templateUrl: './adm-img.component.html',
  styleUrls: ['./adm-img.component.css']
})
export class AdmImgComponent implements OnInit {

  productsImg: ProductsImg[] = [];
  id_producto = '';
  filterUser = '';
  selectedfile: File = null;

  constructor(
    private admImageService: AdmImageService,
    private route: ActivatedRoute,
    public dialog: MatDialog) { }

  ngOnInit() {
    this.getCatalogue();
  }

  getCatalogue() {
    this.admImageService.getImagProducts()
      .subscribe(
        (response) => {
          if (response['error']) {
            switch (response['code']) {
              case 102:

                break;
            }
          } else {
            this.productsImg = response['imgProducts'];
          }
        }
      );
  }

  openDialogProdSus(idProduct) {
    const dialogRef = this.dialog.open(ConfigSustituteProdDialogComponent, {
      width: '90vw',
      height: '90vh',
      data: idProduct
    });

    dialogRef.afterClosed().subscribe(res => {
      if (!res) {
        return;
      }
    });
  }

  onFileChanged(file: any, idProducto: string) {
    if (file.files && file.files[0]) {

      this.selectedfile = file.files[0];
      this.id_producto = idProducto;
      this.onUpload();
    }
  }

  onUpload() {

    this.admImageService.uploadImage(this.selectedfile, this.id_producto)
      .subscribe(
        (response) => {
          if (response['error']) {
            switch (response['code']) {
              case 101:
                this.ShowErrorDialog(response['msg']);
                break;
              case 102:
                this.ShowErrorDialog(response['msg']);
                break;
            }
          } else {
            var infoImage = (Object.values(response));
            this.admImageService.updateRutaImage(this.id_producto, infoImage[0]['file_name'])
              .subscribe(
                (response) => {
                  if (response['error']) {
                    switch (response['code']) {
                      case 101:
                        this.ShowErrorDialog(response['msg']);
                        break;
                      case 102:
                        this.ShowErrorDialog(response['msg']);
                        break;
                      case 103:
                        this.ShowErrorDialog(response['msg']);
                        break;
                      case 104:
                        this.ShowErrorDialog(response['msg']);
                        break;
                    }
                  } else {
                    this.ShowSuccessDialog(response['msg']);
                    this.getCatalogue();
                  }
                }
              );
          }
        }
      );
  }

  ShowErrorDialog(msg: string) {
    Swal.fire({
      title: msg,
      type: 'error',
      showCancelButton: false,
      confirmButtonText: 'Ok, Entiendo',
      confirmButtonColor: '#FF5E4F',
    });
  }

  ShowSuccessDialog(msg: string) {
    Swal.fire({
      title: msg,
      type: 'success',
      showCancelButton: false,
      confirmButtonText: 'Ok',
      confirmButtonColor: '#3085d6',
    });
  }

}
