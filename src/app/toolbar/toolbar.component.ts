import { Component, Output, EventEmitter, Input } from '@angular/core';
import { Categories } from '../classes/categories';
import { FormControl } from '@angular/forms';
import { Product } from '../classes/product';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent {

  @Output() sendText = new EventEmitter();
  @Output() logOut = new EventEmitter();
  searchText = '';
  @Output() goToProducts = new EventEmitter();
  @Output() categorie = new EventEmitter<FormControl>();
  @Input() public productsAddedQuantity: string;
  @Input() public categories: Categories[] = [];


  constructor() { }

  ngOnInit() {

  }

  categori = new FormControl();

  getSelectedCategories(categories: FormControl) {
    this.categorie.emit(categories);
  }


  public getSearch(event: any) {
    this.sendText.emit(event.target.value);
  }

  cancelSearch() {
    this.searchText = '';
    this.sendText.emit('');
  }

  logOutSale() {
    this.logOut.emit();
  }

  redirectToProducts() {
    this.goToProducts.emit();
  }
}
