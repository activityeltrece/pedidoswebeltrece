import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from "../app/login/login.component";
import { DashboardComponent } from "../app/dashboard/dashboard.component";
import { PageNotFoundComponent } from "../app/page-not-found/page-not-found.component";
import { SaleDetailComponent } from "../app/sale-detail/sale-detail.component";
import { AuthGuard } from "../app/services/Guards/auth-guard.service";
import { LoginGuard } from './services/Guards/login-guard.service';
import { HistoricalOrdersComponent } from './historical-orders/historical-orders.component';
import { MainNavComponent } from '../app/main-nav/main-nav.component';
import { AngularMaterialModule } from './material.module';
import { AdmImgComponent } from './adm-img/adm-img.component';
import { ImgAdminGuard } from './services/Guards/img-admin-guard.service';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'Login' },
  { path: 'Login', component: LoginComponent , canActivate: [LoginGuard] },
  { path: 'Venta', component: MainNavComponent , canActivate: [AuthGuard] ,
  children:[
    { path: 'TomaPedido', component: DashboardComponent , canActivate: [AuthGuard] },
    { path: 'HistoricoPedidos', component: HistoricalOrdersComponent , canActivate: [AuthGuard] },
    { path: 'TusProductos', component: SaleDetailComponent , canActivate: [AuthGuard] }
  ]},
  { path: 'GaleriaProducto/:Token', component: AdmImgComponent , canActivate: [ImgAdminGuard] },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash: true}),AngularMaterialModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
