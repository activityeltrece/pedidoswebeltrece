import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { WareHouse } from '../classes/warehouse';
import { OrderLogistic } from '../classes/order_logistic';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AuthService } from '../services/AuthService/auth.service';
import { DashboardService } from '../services/DashboardService/dashboard.service';
import { ToastrService } from 'ngx-toastr';
import * as momentZone from 'moment-timezone';
import * as moment from 'moment';
import Swal from 'sweetalert2'
import { CustomerData } from '../classes/customerData';


@Component({
  selector: 'app-logistic',
  templateUrl: './logistic.component.html',
  styleUrls: ['./logistic.component.css'],
  encapsulation:ViewEncapsulation.None
})
export class LogisticComponent implements OnInit {

  myStyle: object = {};
  myParams: object = {};
  width: number = 100;
  height: number = 100;
  deliverydays: number;
  orderLogistics: OrderLogistic[] = [];
  wareHouses: WareHouse[] = [
    { code: '001', name: 'Planta - CEDI' },
    { code: '002', name: 'Plaza Mayorista' }
  ];
  deliveryDate;
  minDate;

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  constructor(
    private _formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<LogisticComponent>,
    @Inject(MAT_DIALOG_DATA) public customerData: CustomerData,
    private _dashService: DashboardService,
    private _authService: AuthService,
    private toastr: ToastrService) {

  }

  ngOnInit() {
    this.minDate = new Date();
    this.getDeliverydays();
    this.buildForms();
  }

  getDeliverydays() {
    this._dashService.getDeliverydays(this._authService.getDoc())
      .subscribe((response) => {
        if (response['error']) {
          switch (response['code']) {
            case 103:
              this.showFailedToaster(response['msg']);
              break;
            case 104:
              this.showFailedToaster(response['msg']);
              break;
            default:
              break;
          }
        } else {
          this.deliverydays = response['client']['dia_entrega'];
          //si hay dias de entrega se muestra la opcion domicilio del pedido
          if (this.deliverydays) {
            this.orderLogistics = [
              { code: '1', name: 'Recoger pedido' },
              { code: '12', name: 'Domicilio del pedido' }
            ]
            //obtener fecha de entrega en base a el campo dia_entrega del cliente
            const currentDate = momentZone().tz('America/Bogota').format();
            const weekday: number = Number(this.deliverydays);
            const currentDay = moment(currentDate).isoWeekday();

            if (currentDay >= weekday) {
              // Ya que el dia actual es mayor que el dia de la semana que nos llega del cliente, se obtendra la fecha de la proxima semana 
              this.deliveryDate = moment(currentDate).add(1, 'weeks').isoWeekday(weekday).format();
            } else if (currentDay < weekday) {
              // Se obtendra la fecha de la semana actual en base a el dia de entre del cliente
              this.deliveryDate = moment(currentDate).isoWeekday(weekday).format();
            }
          } else {
            this.orderLogistics = [
              { code: '1', name: 'Recoger pedido' }
            ]
          }
        }
      })
  }

  buildForms() {
    this.firstFormGroup = this._formBuilder.group({
      wareHouses: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      orderLogistic: ['', Validators.required],
      collectedDate: ['']
    });

    this.secondFormGroup.get('orderLogistic').valueChanges
      .subscribe((value) => {
        //recoger pedido
        if (value && value === '1') {
          this.secondFormGroup.get('collectedDate').setValidators([Validators.required]);
          this.secondFormGroup.get('collectedDate').updateValueAndValidity({ onlySelf: true, emitEvent: false });
        } else if(value && value === '12'){
          //domicilio del pedido
          this.secondFormGroup.get('collectedDate').setValidators([]);
          this.secondFormGroup.get('collectedDate').setValue('', { onlySelf: true, emitEvent: false });
          this.secondFormGroup.get('collectedDate').updateValueAndValidity({ onlySelf: true, emitEvent: false });
          this.showInfodeliveryDate();
        }
      });
      this.firstFormGroup.get('wareHouses').valueChanges
      .subscribe((value)=>{
        if(value === '002'){
          this.orderLogistics = [
            { code: '1', name: 'Recoger pedido' }
          ]
        }else{
          this.getDeliverydays();
        }
        this.secondFormGroup.get('orderLogistic').setValue('');
        this.secondFormGroup.get('orderLogistic').updateValueAndValidity({ onlySelf: true, emitEvent: false });
      });
  }

  confirmLogistic() {
    let valueLogistic: any;
    const orderLogsistic: number = Number(this.secondFormGroup.value['orderLogistic']);

    switch (orderLogsistic) {
      case 1: //recoger pedido
        valueLogistic = {
          wareHouse: this.firstFormGroup.value['wareHouses'],
          wareHouseName: this.firstFormGroup.value['wareHouses'] === '001' ? 'Planta - CEDI' : 'Plaza Mayorista',
          orderLogistic: orderLogsistic,
          collectedDate: this.secondFormGroup.value['collectedDate'],
          namOrderLogistic: 'Recoger pedido',
          priceList: 1
        }
        break;
      case 12: //domicilio
        valueLogistic = {
          wareHouse: this.firstFormGroup.value['wareHouses'],
          wareHouseName: this.firstFormGroup.value['wareHouses'] === '001' ? 'Planta - CEDI' : 'Plaza Mayorista',
          orderLogistic: orderLogsistic,
          deliveryDate: this.deliveryDate,
          namOrderLogistic: 'Domicilio del pedido',
          priceList: 4
        }
        break;

      default:
        break;
    }

    this.dialogRef.close(valueLogistic);
  }

  logout() {
    this.dialogRef.close('Logout');
  }

  showFailedToaster(msg: string) {
    this.toastr.error(msg);
  }

  showInfodeliveryDate() {
    Swal.fire({
      title: 'Atención',
      text: 'Tu pedido sera entregado en la siguiente fecha: ' + this.deliveryDate.substring(0, 10),
      type: 'info',
      showCloseButton: true,
      showCancelButton: false,
      confirmButtonText: 'Ok, Entiendo',
      confirmButtonColor: '#ef7b13',
    });
  }

  get orderLogistic() { return this.secondFormGroup.get('orderLogistic'); }
}
