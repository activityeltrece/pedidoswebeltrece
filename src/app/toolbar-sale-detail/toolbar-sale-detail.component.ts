import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-toolbar-sale-detail',
  templateUrl: './toolbar-sale-detail.component.html',
  styleUrls: ['./toolbar-sale-detail.component.css']
})
export class ToolbarSaleDetailComponent implements OnInit {

  @Output() backToDashboard = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  redirectToDashboard() {
    this.backToDashboard.emit();
  }

}
