import { AuthService } from '../AuthService/auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map , catchError  } from 'rxjs/operators';
import { EndPointServer } from "../../global.config";

/*
@Injectable({
  providedIn: 'root'
})
*/
export class ImgAdminGuard implements CanActivate {

  constructor(private _authService: AuthService, private httpClient: HttpClient) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.httpClient.post(EndPointServer + "/Auth/AdminImgToken", {
      token: next.params['Token'] ? next.params['Token'] : ''
    }, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    })
    .pipe(
      map(data => {
        if (data) {
          return true;
        } else {
          this._authService.goTo404();
          return false;
        }
      }),
      catchError((err) => {
        this._authService.goTo404();
        return of(false);
      })
    );
  }

}