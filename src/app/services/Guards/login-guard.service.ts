import { AuthService } from '../AuthService/auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map , catchError  } from 'rxjs/operators';
import { EndPointServer } from "../../global.config";

/*@Injectable({
  providedIn: 'root'
})*/
@Injectable()
export class LoginGuard implements CanActivate {

  constructor(private _authService: AuthService, private httpClient: HttpClient) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.httpClient.post(EndPointServer + "/Auth/AuthToken", {
      token: this._authService.getToken() , 
      user: this._authService.getUser() ,
      doc: this._authService.getDoc()
    }, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    })
    .pipe(
      map(data => {
        if (data) {
          this._authService.goToDashboard();
          return false;
        } else {
          return true;
        }
      }),
      catchError((err) => {
        return of(true);
      })
    );
  }

}