import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { EndPointServer } from "../../global.config";

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private httpClient : HttpClient) { }

  public getHistoricalOrders(doc: string){
    return this.httpClient.post(EndPointServer + "/Customer/HistoricalOrder", {doc}, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    })
    .pipe(map(data => data));
  }

  public getCustomerData (doc: string){
    return this.httpClient.post(EndPointServer + "/Customer/CustomerData", {doc}, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    })
    .pipe(map(data => data));
  }

}
