import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { User } from "../../classes/user";
import { EndPointServer } from "../../global.config";

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  baseUrl: string = EndPointServer + "/login";

  constructor(private httpClient : HttpClient) { }

  public Login( user : User) {
    return this.httpClient.post(this.baseUrl, user, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    })
    .pipe(map(data => data));    
  }


  public generateId( user : string ) {
   return this.httpClient.post(this.baseUrl + "/generateId", {user}, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    })
    .pipe(map(data => data));   
  } 

}
