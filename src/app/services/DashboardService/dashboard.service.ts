import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { EndPointServer } from "../../global.config";

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private httpClient : HttpClient) { }

  public getCatalogue(doc: string, idTransaction: string, valueLogistic:number, wareHouse: string){
    return this.httpClient.post(EndPointServer + "/Dashboard/Catalogue", {doc,idTransaction,valueLogistic, wareHouse}, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    })
    .pipe(map(data => data));
  }

  public insertDetail(product: any, user: string, doc: string, idTransaction: string){
    return this.httpClient.post(EndPointServer + "/Dashboard/Description", {product, user, doc, idTransaction}, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    })
    .pipe(map(data => data));
  }

  public deleteProduct(product: any, user: string, doc: string, idTransaction: string){
    return this.httpClient.post(EndPointServer + "/Dashboard/Delete", {product, user, doc, idTransaction}, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    })
    .pipe(map(data => data));
  }

  public updateProduct(product: any, user: string, doc: string, idTransaction: string){
    return this.httpClient.post(EndPointServer + "/Dashboard/Update", {product, user, doc, idTransaction}, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    })
    .pipe(map(data => data));
  }

  public cancelSale(idTransaction: string){
    return this.httpClient.post(EndPointServer + "/Dashboard/CancelSale", {idTransaction}, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    })
    .pipe(map(data => data));
  }

  public getCategories (doc: string){
    return this.httpClient.post(EndPointServer + "/Dashboard/Categories", {doc}, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    })
    .pipe(map(data => data));
  }

  public getDeliverydays (doc: string){
    return this.httpClient.post(EndPointServer + "/Dashboard/Deliverydays", {doc}, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    })
    .pipe(map(data => data));
  }

}
