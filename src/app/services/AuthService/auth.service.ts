import { Injectable } from '@angular/core';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  TokenKey: string = 'AC'; //TokenUsuario
  UserKey: string = 'ID'; //CedulaUsuario
  DocKey: string = 'D'; //CedulaCliente
  IdTransaction: string = 'IT'; //IdPedido
  sesionLogistic: string = 'SL';//Validar si hay logistica seleccionada
  valueLogistic: string = 'VL'//valorLogistica(Recoger,Domicilio)
  wereHouse: string = 'WH'; //Almacen
  wareHouseName: string = 'WHN'; //Nombre Almacen
  namOrderLogistic: string = 'NO'; //Nombre order logistica
  dateLogistic: string = 'DL'; //Fecha logistica
  constructor(private router: Router) {

  }

  loginAndRedirectDashboard(token: string, user: string, doc: string, idTransaction: string) {
    localStorage.setItem(this.TokenKey, token);
    localStorage.setItem(this.UserKey, user);
    localStorage.setItem(this.DocKey, doc);
    localStorage.setItem(this.IdTransaction, idTransaction);
    this.goToDashboard();
  }

  FinishSaleAndRedirectVentas(token: string, user: string, doc: string, idTransaction: string) {
    localStorage.removeItem(this.sesionLogistic);
    localStorage.removeItem(this.valueLogistic);
    localStorage.removeItem(this.wereHouse);
    localStorage.removeItem(this.namOrderLogistic);
    localStorage.removeItem(this.dateLogistic);
    localStorage.removeItem(this.wareHouseName);
    localStorage.setItem(this.TokenKey, token);
    localStorage.setItem(this.UserKey, user);
    localStorage.setItem(this.DocKey, doc);
    localStorage.setItem(this.IdTransaction, idTransaction);
    this.goToDashboard();
  }

  public sesionLogisticc() {
    localStorage.setItem(this.sesionLogistic, '1');
  }

  public valueLogisticc(valueLogistic) {
    localStorage.setItem(this.valueLogistic, valueLogistic);
  }

  public setWereHouse(wereHouse: string) {
    localStorage.setItem(this.wereHouse, wereHouse);
  }

  public setWareHouseName(wareHouseName: string) {
    localStorage.setItem(this.wareHouseName, wareHouseName);
  }

  public setDateLogistic(dateLogistic: string) {
    localStorage.setItem(this.dateLogistic, dateLogistic);
  }

  public seTNamOrderLogistic(namOrderLogistic: string) {
    localStorage.setItem(this.namOrderLogistic, namOrderLogistic);
  }

  clearData() {
    localStorage.removeItem(this.TokenKey);
    localStorage.removeItem(this.UserKey);
    localStorage.removeItem(this.DocKey);
    localStorage.removeItem(this.IdTransaction);
    localStorage.removeItem(this.sesionLogistic);
    localStorage.removeItem(this.valueLogistic);
    localStorage.removeItem(this.wereHouse);
    localStorage.removeItem(this.namOrderLogistic);
    localStorage.removeItem(this.dateLogistic);
    localStorage.removeItem(this.wareHouseName);
  }

  goToDashboard() {
    this.router.navigate(['Venta/TomaPedido']);
  }


  goToLogin() {
    this.clearData();
    this.router.navigate(['Login']);
  }

  goTo404() {
    this.router.navigate(['InvalidRequest']);
  }

  public isLogIn(): boolean {
    return ((localStorage.getItem(this.TokenKey) !== null) && (localStorage.getItem(this.UserKey) !== null));
  }

  public getToken(): string {
    if (this.isLogIn()) {
      return localStorage.getItem(this.TokenKey);
    } else {
      return "";
    }
  }

  public getUser(): string {
    if (this.isLogIn()) {
      return localStorage.getItem(this.UserKey);
    } else {
      return "";
    }
  }

  public getDoc(): string {
    if (this.isLogIn()) {
      return localStorage.getItem(this.DocKey);
    } else {
      return "";
    }
  }

  public getTransactionId(): string {
    if (this.isLogIn()) {
      return localStorage.getItem(this.IdTransaction);
    } else {
      return "";
    }
  }

  public getsesionLogistic(): string {
    if (this.isLogIn()) {
      return localStorage.getItem(this.sesionLogistic);
    } else {
      return "";
    }
  }

  public getValueLogistic(): string {
    if (this.isLogIn()) {
      return localStorage.getItem(this.valueLogistic);
    } else {
      return "";
    }
  }
  public getWereHouse(): string {
    if (this.isLogIn()) {
      return localStorage.getItem(this.wereHouse);
    } else {
      return "";
    }
  }

  public getWareHouseName(): string {
    if (this.isLogIn()) {
      return localStorage.getItem(this.wareHouseName);
    } else {
      return "";
    }
  }

  public getDateLogistic(): string {
    if (this.isLogIn()) {
      return localStorage.getItem(this.dateLogistic);
    } else {
      return "";
    }
  }

  public getNamOrderLogistic(): string {
    if (this.isLogIn()) {
      return localStorage.getItem(this.namOrderLogistic);
    } else {
      return "";
    }
  }

}
