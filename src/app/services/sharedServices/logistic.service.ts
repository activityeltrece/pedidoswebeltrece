import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { AuthService } from '../AuthService/auth.service';

@Injectable({
  providedIn: 'root'
})

export class LogisticService {

  private $wareHouseName = new Subject<string>();
  private $dateLogistic = new Subject<string>();
  private $namOrderLogistic = new Subject<string>();
  constructor(private authService: AuthService) { }

  setWareHouseName() {
    this.$wareHouseName.next(this.authService.getWareHouseName());
  }

  setDateLogistic() {
    this.$dateLogistic.next(this.authService.getDateLogistic());
  }

  setNamOrderLogistic() {
    this.$namOrderLogistic.next(this.authService.getNamOrderLogistic());
  }

  getWareHouseName() {
    return this.$wareHouseName.asObservable();
  }

  getDateLogistic() {
    return this.$dateLogistic.asObservable();
  }

  getNamOrderLogistic() {
    return this.$namOrderLogistic.asObservable();
  }

}

export interface Logistic {
  orderLogistics: string;
  deliveryLogistics: string;
  date: Date;
}
