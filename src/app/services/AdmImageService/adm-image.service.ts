import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { EndPointServer } from "../../global.config";
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AdmImageService {

  constructor(private httpClient: HttpClient) {
  }

  public getImagProducts() {
    return this.httpClient.post(EndPointServer + "/Dash_AdmImag/ImgProducts", {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    })
      .pipe(map(data => data));
  }

  public uploadImage(mi_archivo: File, idP: string) {
    const formData: FormData = new FormData();
    formData.append('mi_archivo', mi_archivo, mi_archivo.name);
    formData.append('idP', idP);
    return this.httpClient.post(EndPointServer + "/Archive/uploadImage", formData)
      .pipe(map(data => data));
  }

  public updateRutaImage(idP: string, nameImage: string) {

    return this.httpClient.post(EndPointServer + "/Archive/updateRutaImage", { idP, nameImage }, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    })
      .pipe(map(data => data));
  }

  public sustituteProds(idProduct: number): Observable<any> {
    return this.httpClient.post<any>(EndPointServer + "/Archive/sustituteProds", { idProduct }, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    });
  }

  public removeSusProd(idSustProd: number): Observable<any> {
    return this.httpClient.post<any>(EndPointServer + "/Archive/removeSusProd", { idSustProd }, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    });
  }

  public allProducts(): Observable<any> {
    return this.httpClient.get<any>(EndPointServer + "/Archive/allProducts", {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    });
  }

  public insertSusProd(idProduct:number, idSustProd): Observable<any> {
    return this.httpClient.post<any>(EndPointServer + "/Archive/insertSusProd", {idProduct, idSustProd}, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    });
  }
}

export interface sustituteProd {
  idSustProd: number,
  descp: string,
  PathImage: string,
  ImageDesc: string
}

export interface Products {
  idProduct: number,
  descp: string
}
