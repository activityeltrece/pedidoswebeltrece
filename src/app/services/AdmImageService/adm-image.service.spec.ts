import { TestBed } from '@angular/core/testing';

import { AdmImageService } from './adm-image.service';

describe('AdmImageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdmImageService = TestBed.get(AdmImageService);
    expect(service).toBeTruthy();
  });
});
