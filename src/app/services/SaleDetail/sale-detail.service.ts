import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { EndPointServer } from "../../global.config";
import { SaleHeader } from 'src/app/classes/sale_header';

@Injectable({
  providedIn: 'root'
})
export class SaleDetailService {

  constructor(private httpClient: HttpClient) { }

  public getSaleDetail(idTransaction: string) {
    return this.httpClient.post(EndPointServer + "/Sale/Detail", { idTransaction }, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    })
      .pipe(map(data => data));
  }

  public finishSale(idTransaction: string, updatedHeader: SaleHeader) {
    return this.httpClient.post(EndPointServer + "/Sale/Finish", { idTransaction, updatedHeader }, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    })
      .pipe(map(data => data));
  }

  public updateLogisticDate(idTransaction: string, logisticDate: Date, docTypePsl: string, deliveryLogistics: string, priceGroup: number) {
    return this.httpClient.post(EndPointServer + "/Sale/updateLogisticDate", { idTransaction, logisticDate, docTypePsl, deliveryLogistics, priceGroup }, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    })
      .pipe(map(data => data));
  }

  public updateLogistiDate(idTransaction: string, logisticDate: Date, deliveryLogistics: string, priceGroup: number) {
    return this.httpClient.post(EndPointServer + "/Sale/updateLogistiDate", { idTransaction, logisticDate, deliveryLogistics, priceGroup }, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    })
      .pipe(map(data => data));
  }
}
