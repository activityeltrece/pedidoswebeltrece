import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SustituteProdDialogComponent } from './sustitute-prod-dialog.component';

describe('SustituteProdDialogComponent', () => {
  let component: SustituteProdDialogComponent;
  let fixture: ComponentFixture<SustituteProdDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SustituteProdDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SustituteProdDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
