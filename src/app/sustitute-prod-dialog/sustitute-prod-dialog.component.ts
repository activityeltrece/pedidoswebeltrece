import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ConfigSustituteProdDialogComponent } from '../config-sustitute-prod-dialog/config-sustitute-prod-dialog.component';
import { ToastrService } from 'ngx-toastr';
import { Product } from '../classes/product';
import { DashboardService } from '../services/DashboardService/dashboard.service';
import { AuthService } from '../services/AuthService/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sustitute-prod-dialog',
  templateUrl: './sustitute-prod-dialog.component.html',
  styleUrls: ['./sustitute-prod-dialog.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SustituteProdDialogComponent implements OnInit {

  productsAdded: string = '0';
  isSustitute: boolean = false;

  constructor(
    public dialogRef: MatDialogRef<ConfigSustituteProdDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public products: Product[],
    private dataService: DashboardService,
    private authService: AuthService,
    private toastr: ToastrService,
    private router: Router,
  ) { }

  ngOnInit() {
  }

  insertProduct(product) {
    this.dataService.insertDetail(product[0], this.authService.getUser(), this.authService.getDoc(), this.authService.getTransactionId())
      .subscribe(
        (response) => {
          if (response['error']) {
            switch (response['code']) {
              case 103:
                this.showFailedToaster(response['msg']);
                break;
              case 104:
                this.showFailedToaster(response['msg']);
                break;
              case 105:
                this.showInfoToaster(response['msg']);
                break;
              case 106:
                this.showFailedToaster(response['msg']);
                break;
              default:
                this.showInfoToaster(response['msg']);
            }
          } else {
            this.showSuccessToaster(response['msg']);
            let itemToUpdate = this.products.findIndex(
              productList => productList.IdProducto == product[1].IdProducto
            );
            this.products[itemToUpdate] = product[1];
            this.refreshQuantityAdded();
          }
        }
      );
  }

  deleteProduct(product) {
    this.dataService.deleteProduct(product[0], this.authService.getUser(), this.authService.getDoc(), this.authService.getTransactionId())
      .subscribe(
        (response) => {
          if (response['error']) {
            this.showFailedToaster(response['msg']);
          } else {
            this.showSuccessToaster(response['msg']);
            let itemToUpdate = this.products.findIndex(
              productList => productList.IdProducto == product[1].IdProducto
            );
            this.products[itemToUpdate] = product[1];
            this.refreshQuantityAdded();
          }
        }
      );
  }

  private refreshQuantityAdded() {
    this.productsAdded = this.products.map(product => product.Presentaciones)
      .reduce((totalQuantity, quantity) =>
        String(Number(totalQuantity) +
          Number(
            quantity.reduce((quantityAllPresentations, presentationSingle) =>
              String(Number(quantityAllPresentations) + (Number(presentationSingle.CantidadAgregada) > 0 ? 1 : 0))
              , '0')
          )
        )
        , '0');
  }

  closeDialog(): void {
    // const productsAdded: number = Number(this.productsAdded);
    // let isProductAdd: boolean = false;
    // if (Number(productsAdded > 0)) {
    //   isProductAdd = true;
    // }
    this.dialogRef.close();
  }

  showFailedToaster(msg: string) {
    this.toastr.error(msg);
  }

  showInfoToaster(msg: string) {
    this.toastr.warning(msg);
  }

  showSuccessToaster(msg: string) {
    this.toastr.success(msg);
  }

}
