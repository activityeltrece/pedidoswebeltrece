import { Component, OnInit } from '@angular/core';
import { AuthService } from "../services/AuthService/auth.service";
import Swal from 'sweetalert2'
import { ToastrService } from 'ngx-toastr';
import { CustomerService } from '../services/CustomerService/customer.service';
import { HistoricalOrders } from '../classes/historicalOrders';
import { detailOrder } from '../classes/detail_order';

@Component({
  selector: 'app-historical-orders',
  templateUrl: './historical-orders.component.html',
  styleUrls: ['./historical-orders.component.css']
})
export class HistoricalOrdersComponent implements OnInit {

  constructor(private customerService: CustomerService, private authService: AuthService, private toastr: ToastrService) { }

  historicalOrders: HistoricalOrders[] = [];
  detailOrderr: detailOrder[] = [];
  displayedColumns = ['descripcion','UnidadPresentacion','cantidad','valor_unitario','vlr_iva','total_pedido_articulo'];

  ngOnInit() {
    this.customerService.getHistoricalOrders(this.authService.getDoc())
      .subscribe(
        (response) => {
          if (response['error']) {
            switch (response['code']) {
              case 101:
                this.ShowErrorDialog(response['msg']); //Error en información de usuario.
                this.authService.goToLogin();
                break;
              case 102:
                this.ShowWarningDialog(response['msg']); //No hay historial de pedidos vinculados a tu información de usuario.
                break;
            }
          } else {
            this.historicalOrders = response['historicalOrders'];
          }
        }
      );
  }

  showDetailsOrder(detailOrders : HistoricalOrders){
    this.detailOrderr = detailOrders.ProductsDetail;
  }

  getTotalCost() {
    return this.detailOrderr.map(t => t.cantidad).reduce((acc, value) => acc + Number(value), 0);
  }

  ShowWarningDialog(msg: string) {
    Swal.fire({
      title: 'Atención',
      text: msg,
      type: 'warning',
      showCancelButton: false,
      confirmButtonText: 'Ok, Entiendo',
      confirmButtonColor: '#FF5E4F',
    });
  }

  ShowErrorDialog(msg: string) {
    Swal.fire({
      title: 'Atención',
      text: msg,
      type: 'error',
      showCancelButton: false,
      confirmButtonText: 'Ok, Entiendo',
      confirmButtonColor: '#FF5E4F',
    });
  }

  redirectToDashboard() {
    this.authService.goToDashboard();
  }
}




