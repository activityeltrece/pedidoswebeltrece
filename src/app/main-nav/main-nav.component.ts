import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { CustomerService } from '../services/CustomerService/customer.service';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2'
import { AuthService } from '../services/AuthService/auth.service';
import { CustomerData } from '../classes/customerData';
import { DashboardService } from '../services/DashboardService/dashboard.service';
import { LogisticService, Logistic } from '../services/sharedServices/logistic.service';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.css']
})
export class MainNavComponent {

  customerData: CustomerData;
  logistic: Logistic;
  wareHouseName: string;
  nameOrderLogistic: string;
  dateLogistic: string;
  constructor(
    private breakpointObserver: BreakpointObserver,
    private customerService: CustomerService,
    private toastr: ToastrService,
    private authService: AuthService,
    private dataService: DashboardService,
    private _logisticService: LogisticService) { }

  ngOnInit() {
    this.getCustomerData();
    this._logisticService.getWareHouseName()
      .subscribe(data => this.wareHouseName = data);
    this._logisticService.getDateLogistic()
      .subscribe(data => this.dateLogistic = data);
    this._logisticService.getNamOrderLogistic()
      .subscribe(data => this.nameOrderLogistic = data);
  }

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  getCustomerData() {
    this.customerService.getCustomerData(this.authService.getDoc())
      .subscribe(
        (response) => {
          if (response['error']) {
            switch (response['code']) {
              case 101:
                this.ShowErrorDialog(response['msg']); //Error en información de usuario.
                this.authService.goToLogin();
                break;
              case 102:
                this.ShowWarningDialog(response['msg']); //No hay datos del cliente vinculados a tu información de usuario.
                break;
            }
          } else {
            this.customerData = response['customerDat'][0];
          }
        }
      );
  }

  logOutSale() {
    this.dataService.cancelSale(this.authService.getTransactionId())
      .subscribe((response) => {
        if (response['error']) {
          this.showFailedToaster(response['msg']);
        } else {
          this.showSuccessToaster(response['msg']);
          this.authService.goToLogin();
        }
      });
  }

  showSuccessToaster(msg: string) {
    this.toastr.success(msg);
  }

  showInfoToaster(msg: string) {
    this.toastr.warning(msg);
  }

  showFailedToaster(msg: string) {
    this.toastr.error(msg);
  }

  ShowErrorDialog(msg: string) {
    Swal.fire({
      title: 'Atención',
      text: msg,
      type: 'error',
      showCancelButton: false,
      confirmButtonText: 'Ok, Entiendo',
      confirmButtonColor: '#FF5E4F',
    });
  }

  ShowWarningDialog(msg: string) {
    Swal.fire({
      title: 'Atención',
      text: msg,
      type: 'warning',
      showCancelButton: false,
      confirmButtonText: 'Ok, Entiendo',
      confirmButtonColor: '#FF5E4F',
    });
  }
}
