import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2'
import { LoginService } from "../services/LoginService/login.service";
import { AuthService } from "../services/AuthService/auth.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  myStyle: object = {};
  myParams: object = {};
  width: number = 100;
  height: number = 100;
  logging: boolean = false;

  constructor(private formBuilder: FormBuilder, private dataService: LoginService, private authService: AuthService) { }

  ngOnInit() {
    this.authService.clearData();
    this.myStyle = {
      'position': 'fixed',
      'width': '100%',
      'height': '100%',
      'z-index': -1,
      'top': 0,
      'left': 0,
      'right': 0,
      'bottom': 0,
    };
    this.myParams = {
      particles: {
        number: {
          value: 200,
          density: {
            enable: true,
            value_area: 800
          }
        },
        color: {
          value: ["#ef7b13", "#2e9e31", "#FAD008", "#FF5E4F"]
        },
        shape: {
          type: 'triangle',
          stroke: {
            width: 0,
            color: "#ef7b13"
          }
        },
        opacity: {
          value: 0.5211089197812949,
          random: false,
          anim: {
            enable: true,
            speed: 1,
            opacity_min: 0.1,
            sync: false
          }
        },
        size: {
          value: 8.017060304327615,
          random: true,
          anim: {
            enable: true,
            speed: 12.181158184520175,
            size_min: 0.1,
            sync: true
          }
        },
        line_linked: {
          enable: true,
          distance: 150,
          color: "#2e9e31",
          opacity: 0.4,
          width: 1
        },
        move: {
          enable: true,
          speed: 1,
          direction: "none",
          random: false,
          straight: false,
          out_mode: "bounce",
          bounce: false,
          attract: {
            enable: false,
            rotateX: 600,
            rotateY: 1200
          }
        }
      }
    };

    this.loginForm = this.formBuilder.group({
      user: ['', Validators.required],
      password: ['', Validators.required]
    });

  }

  Login() {
    if (this.loginForm.invalid) {
      this.ShowWarningDialog('Debes introducir tus credenciales para ingresar a la plataforma.');
      return;
    }
    this.logging = true;
    this.dataService.Login({ "user": this.loginForm.value.user, "pass": this.loginForm.value.password }).subscribe(
      (response) => {
        if (response['error']) {
          this.ShowErrorDialog(response['msg']);
        } else {
          this.authService.loginAndRedirectDashboard(response['token'], this.loginForm.value.user, response['doc'], response['TransactionID']);
        }
        this.logging = false;
      }
    );
  }

  ShowWarningDialog(msg: string) {
    Swal.fire({
      title: 'Atención',
      text: msg,
      type: 'warning',
      showCancelButton: false,
      confirmButtonText: 'Ok, Entiendo',
      confirmButtonColor: '#FF5E4F',
    });
  }

  ShowErrorDialog(msg: string) {
    Swal.fire({
      title: 'Atención',
      text: msg,
      type: 'error',
      showCancelButton: false,
      confirmButtonText: 'Ok, Entiendo',
      confirmButtonColor: '#FF5E4F',
    });
  }

}
